import Gradle.buildConfig

plugins {
    id("com.android.application")
    kotlin(Gradle.plugins.android)
    kotlin(Gradle.plugins.androidExtensions)
    kotlin(Gradle.plugins.kapt)
    id("kotlin-android")
}

android {

    compileSdkVersion(Versions.compileSdk)
    ndkVersion = Gradle.versions.ndk

    packagingOptions {
        exclude ("META-INF/DEPENDENCIES")
        exclude ("META-INF/LICENSE")
        exclude ("META-INF/LICENSE.txt")
        exclude ("META-INF/license.txt")
        exclude ("META-INF/NOTICE")
        exclude ("META-INF/NOTICE.txt")
        exclude ("META-INF/notice.txt")
        exclude ("META-INF/ASL2.0")
        exclude ("META-INF/project.properties")
        exclude ("META-INF/MANIFEST.MF")
        exclude ("META-INF/rxjava.properties")
    }

    defaultConfig {
        applicationId = "com.ams.currencyconverter"
        minSdkVersion(Gradle.versions.minSdk)
        targetSdkVersion(Gradle.versions.targetSdk)
        buildToolsVersion = Gradle.versions.buildTools
        versionCode = Gradle.build.versionCode
        versionName = Gradle.build.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    flavorDimensions(buildConfig.serverMode)

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = Gradle.versions.jvmTarget
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
    }

    defaultConfig {
        buildConfigField(buildConfig.stringField, buildConfig.databaseName, "\"CurrencyConverterDB\"")
        buildConfigField(buildConfig.stringField, buildConfig.baseCountriesFlagsUrl, "\"https://restcountries.eu/rest/v2/all?fields=currencies;flag\"")
        buildConfigField(buildConfig.stringField, buildConfig.baseUrlField, "\"http://api.currencylayer.com/\"")
        buildConfigField(buildConfig.stringField, buildConfig.apiKey, "\"a9e44eeefa59c1946c018e17f72b87fd\"")
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${rootProject.extra["kotlin_version"]}")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    // Unit testing
    androidTestImplementation(Gradle.dependencies.testRunner)
    testImplementation(Gradle.dependencies.mockito)
    testImplementation(Gradle.dependencies.koinUnitTest)
    //
    implementation(Gradle.dependencies.stdlib)
    implementation(Gradle.dependencies.ktxCore)
    implementation(Gradle.dependencies.appCompat)
    implementation(Gradle.dependencies.material)
    implementation(Gradle.dependencies.constraintLayout)
    implementation(Gradle.dependencies.androidDevKit)
    // Rx
    implementation(Gradle.dependencies.rxJava)
    implementation(Gradle.dependencies.rxAndroid)
    // Room
    kapt(Gradle.dependencies.roomKAPT)
    implementation(Gradle.dependencies.room)
    implementation(Gradle.dependencies.roomRx)
    // Koin DI
    implementation(Gradle.dependencies.koin)
    implementation(Gradle.dependencies.koinViewModel)
    implementation(Gradle.dependencies.koinFragment)
    // Work manager
    implementation(Gradle.dependencies.rxWorkManager)
    //
    implementation(Gradle.dependencies.retrofitGsonAdapter)
    // Image loader
    implementation(Gradle.dependencies.coil)
    implementation(Gradle.dependencies.coilSVG)
}