package com.ams.currencyconverter.backgroundFetchers

import android.content.Context
import androidx.work.*
import androidx.work.rxjava3.RxWorker
import com.ams.currencyconverter.common.application.Application
import com.ams.currencyconverter.common.di.KoinComponentResolver
import com.ams.currencyconverter.ui.views.common.CurrenciesListingRepo
import io.reactivex.rxjava3.core.Single
import org.koin.core.inject
import java.util.concurrent.TimeUnit

class DataUpdateWorker(context: Context, workerParams: WorkerParameters): RxWorker(context, workerParams) {

    private val currenciesListingRepo: CurrenciesListingRepo by KoinComponentResolver.inject()

    override fun createWork(): Single<Result> {
        /*
            1. The createWork() method is called on the main thread but the returned Single subscribes on the background thread.
            2. We don’t need to worry about disposing the Observer since RxWorker will dispose it automatically when the work stops.
            3. Returning Single with the value Result.failure() or single with an error will cause the worker to enter the failed state.
         */
        return currenciesListingRepo
            .getCurrenciesRatesWithFlags(true)
            .ignoreElement()
            .andThen(Single.just(Result.success()))
    }

    companion object {

        private const val TAG = "DataUpdateWorker"

        fun run() {
            val constraints = Constraints
                .Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()
            val periodicRequest = PeriodicWorkRequest
                .Builder(DataUpdateWorker::class.java, 30, TimeUnit.MINUTES)
                .addTag(TAG)
                .setConstraints(constraints)
                .build()
            WorkManager
                .getInstance(Application.instance)
                .enqueueUniquePeriodicWork(TAG, ExistingPeriodicWorkPolicy.REPLACE, periodicRequest)
        }
    }
}