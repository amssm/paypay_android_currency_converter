package com.ams.currencyconverter.ui.views.covertCurrencyFragment

import androidx.core.widget.doOnTextChanged
import com.ams.androiddevkit.utils.extensions.tryCastListOf
import com.ams.currencyconverter.R
import com.ams.currencyconverter.common.baseClasses.mvvm.AppFragment
import com.ams.currencyconverter.common.data.models.CountryInfo
import com.ams.currencyconverter.common.extensions.setDivider
import com.ams.currencyconverter.ui.views.common.recyclerView.CurrenciesListingAdapter
import com.ams.currencyconverter.ui.views.fragmentHostActivity.SharedViewModel
import kotlinx.android.synthetic.main.fragment_convert_currency.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ConvertCurrencyFragment: AppFragment<ConvertCurrencyViewModel>(ConvertCurrencyViewModel::class) {

    private val sharedViewModel: SharedViewModel by sharedViewModel()
    private val recyclerViewAdapter by lazy { CurrenciesListingAdapter() }

    override fun getViewId() = R.layout.fragment_convert_currency

    override fun initUI() {
        sharedViewModel.getChosenCurrencyInfo().apply {
            chosen_currency_text_view.text = currencyName
            getViewModel()?.setChosenCurrencyInfo(currencyName, currencyRate)
        }
        recycler_view.apply {
            adapter = recyclerViewAdapter
            setDivider(R.drawable.recycler_view_divider)
        }
        chosen_currency_edit_text.doOnTextChanged { text, _, _, _ ->
            getViewModel()?.onUserInput(text)
        }
    }

    override fun onRenderStateWithResult(obj: Any?) {
        obj.tryCastListOf<CountryInfo> {
            recyclerViewAdapter.apply {
                clear()
                addAll(this@tryCastListOf)
            }
        }
    }
}


