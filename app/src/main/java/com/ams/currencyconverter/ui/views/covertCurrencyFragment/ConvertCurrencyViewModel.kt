package com.ams.currencyconverter.ui.views.covertCurrencyFragment

import androidx.lifecycle.LifecycleOwner
import com.ams.androiddevkit.utils.extensions.applyThreadingConfig
import com.ams.currencyconverter.common.baseClasses.mvvm.AppViewModel
import com.ams.currencyconverter.common.data.models.CountryInfo
import com.ams.currencyconverter.ui.views.common.CurrenciesListingRepo
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class ConvertCurrencyViewModel(private val currenciesListingRepo: CurrenciesListingRepo): AppViewModel() {

    private var conversionRate: Float = 0F
    // This flag is used to force the conversion to be done locally instead of the API, if an error is caused by the API.
    // In this case, this variable is set to true to save network hits that will be done by future conversions attempts.
    private lateinit var chosenCurrencySymbol: String
    private val publishSubject = PublishSubject.create<CharSequence>()


    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        this.observeUserInput()
    }

    fun setChosenCurrencyInfo(currency: String, conversionRate: Float) {
        this.chosenCurrencySymbol = currency
        this.conversionRate = conversionRate
    }

    fun onUserInput(text: CharSequence?) {
        this.publishSubject.onNext(text)
    }

    private fun observeUserInput() {
        publishSubject
            .map(CharSequence::toString)
            .map { if(it.isNullOrEmpty()) { 0.0F } else it.toFloat() }
            .debounce(500, TimeUnit.MILLISECONDS)
            .flatMap { this.getConvertedRatesListObservable(it) }
            .applyThreadingConfig()
            .also {
                requestExecuter.execute(it, withLoader = false)
            }
    }

    private fun getConvertedRatesListObservable(userInput: Float): Observable<List<CountryInfo>> {
        return currenciesListingRepo
                .getCurrenciesRatesWithFlags()
                .toObservable()
                .flatMap { Observable.fromIterable(it) }
                .filter { it.currencyName != chosenCurrencySymbol }
                .toList()
                .map { it.toList() }
                .map { it.map { countryInfo -> countryInfo.with(userInput, this.conversionRate) } }
                .toObservable()
    }
}