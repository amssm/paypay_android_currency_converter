package com.ams.currencyconverter.ui.views.common

import com.ams.currencyconverter.common.Constants
import com.ams.currencyconverter.common.data.database.daos.CountriesInfoDAO
import com.ams.currencyconverter.common.data.models.CountryInfo
import com.ams.currencyconverter.common.data.rest.apis.APIs
import com.ams.currencyconverter.common.exceptionsAndThrowables.APIError
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

class CurrenciesListingRepo(private val apIs: APIs, private val countriesInfoDAO: CountriesInfoDAO) {

    fun getCurrenciesRatesWithFlags(forceNetwork: Boolean = false): Single<List<CountryInfo>> {
        if(forceNetwork) {
            return getRemoteCurrenciesRatesWithFlags().flatMap { remoteList ->
                countriesInfoDAO
                    .addAll(remoteList)
                    .andThen(Single.just(remoteList))
            }
        }
        return countriesInfoDAO.getAll().flatMapSingle { localList ->
            if(localList.isEmpty() ) {
                getRemoteCurrenciesRatesWithFlags().flatMap { remoteList ->
                    countriesInfoDAO
                            .addAll(remoteList)
                            .andThen(Single.just(remoteList))
                }
            }
            else {
                Single.just(localList)
            }
        }.toSingle()
    }

    private fun getRemoteCurrenciesRatesWithFlags(): Single<List<CountryInfo>> {
        val countries = mutableListOf<CountryInfo>()
        var countryInfo: CountryInfo
        val observable = Observable.combineLatest(
            getCurrenciesWithFlagsObservable(),
            getCurrenciesWithRatesObservable(), { listWithFlags, currenciesWithRatesMap ->
            currenciesWithRatesMap?.forEach { (key, value) ->
                countryInfo = CountryInfo(key, value)
                countryInfo.flag = listWithFlags.find { it.hasCurrencyWithSymbol(key) }?.flag
                countries.add(countryInfo)
            }
            countries.toList().sortedBy { it.currencyName }
        })
        return Single.fromObservable(observable)
    }

    private fun getCurrenciesWithFlagsObservable() = apIs.getCountriesFlags()

    private fun getCurrenciesWithRatesObservable() = apIs.getCountriesCurrencies().flatMap { response ->
        if (!response.success) {
            Observable.error(APIError(success = false, response.error!!))
        }
        else
            Observable.just(response.quotes).map { quotes ->
                return@map quotes?.mapKeys { it.key.replaceFirst(Constants.BASE_CURRENCY, "") }
            }
    }
}