package com.ams.currencyconverter.ui.views.currenciesListingFragment

import androidx.lifecycle.LifecycleOwner
import com.ams.currencyconverter.common.baseClasses.mvvm.AppViewModel
import com.ams.currencyconverter.common.data.models.CountryInfo
import com.ams.currencyconverter.ui.views.common.CurrenciesListingRepo
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class CurrenciesListingViewModel(private val currenciesListingRepo: CurrenciesListingRepo): AppViewModel() {

    private lateinit var listOfCountriesWithInfo: List<CountryInfo>
    private val searchPublishSubject = PublishSubject.create<String>()

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        this.listenForSearchText()
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        if(::listOfCountriesWithInfo.isInitialized) {
            this.viewStateController.postResult(listOfCountriesWithInfo)
        }
        else {
            this.requestExecuter.execute(currenciesListingRepo.getCurrenciesRatesWithFlags().doOnSuccess {
                listOfCountriesWithInfo = it
            })
        }
    }

    fun onUserInput(text: String) {
        this.searchPublishSubject.onNext(text)
    }

    private fun listenForSearchText() {
        searchPublishSubject
            // Skip 1 because I want to skip the initial.
            .skip(1)
            .debounce(500, TimeUnit.MILLISECONDS)
            .flatMap {
                if (it.isNullOrEmpty()) {
                    Observable.just(listOfCountriesWithInfo)
                }
                else {
                    Observable.just(listOfCountriesWithInfo.filter {
                        countryInfo -> countryInfo.currencyName.contains(it, ignoreCase = true)
                    })
                }
            }
            .apply {
                requestExecuter.execute(this, withLoader = false)
            }
    }
}