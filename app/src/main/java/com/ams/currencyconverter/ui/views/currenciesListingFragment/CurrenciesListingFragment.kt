package com.ams.currencyconverter.ui.views.currenciesListingFragment

import android.annotation.SuppressLint
import androidx.appcompat.widget.SearchView
import com.ams.androiddevkit.utils.extensions.tryCast
import com.ams.androiddevkit.utils.extensions.tryCastListOf
import com.ams.currencyconverter.R
import com.ams.currencyconverter.common.Constants
import com.ams.currencyconverter.common.baseClasses.mvvm.AppFragment
import com.ams.currencyconverter.common.data.models.CountryInfo
import com.ams.currencyconverter.common.exceptionsAndThrowables.APIError
import com.ams.currencyconverter.common.extensions.setDivider
import com.ams.currencyconverter.ui.dialogs.AlertDialogFragment
import com.ams.currencyconverter.ui.views.common.recyclerView.CurrenciesListingAdapter
import com.ams.currencyconverter.ui.views.fragmentHostActivity.SharedViewModel
import kotlinx.android.synthetic.main.fragment_currencies_listing.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class CurrenciesListingFragment: AppFragment<CurrenciesListingViewModel>(CurrenciesListingViewModel::class) {

    private lateinit var recyclerViewAdapter: CurrenciesListingAdapter
    private val sharedViewModel: SharedViewModel by sharedViewModel()

    override fun getViewId() = R.layout.fragment_currencies_listing

    @SuppressLint("SetTextI18n")
    override fun initUI() {
        base_currency_image_view.text = getString(R.string.base_currency) + " : " + Constants.BASE_CURRENCY
        this.initRecyclerView()
        this.initSearchBarListener()
    }

    override fun onRenderStateWithResult(obj: Any?) {
        obj.tryCastListOf<CountryInfo> {
            recyclerViewAdapter.apply {
                clear()
                addAll(this@tryCastListOf)
            }
        }
    }

    override fun onRenderThrowableState(throwable: Throwable) {
        throwable.tryCast<APIError> ({
            AlertDialogFragment.showError(requireActivity(), this.getErrorMessage())
        }, fallback = {
            super.onRenderThrowableState(throwable)
        })
    }

    private fun initRecyclerView() {
        recyclerViewAdapter = CurrenciesListingAdapter {
            sharedViewModel.renderCurrencyConverterView(it)
        }
        recycler_view.apply {
            adapter = recyclerViewAdapter
            setDivider(R.drawable.recycler_view_divider)
        }
    }

    private fun initSearchBarListener() {
        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                getViewModel()?.onUserInput(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }
        })
    }
}