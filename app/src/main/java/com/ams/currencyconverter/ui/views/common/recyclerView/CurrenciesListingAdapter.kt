package com.ams.currencyconverter.ui.views.common.recyclerView

import android.view.ViewGroup
import com.ams.androiddevkit.baseClasses.recyclerView.BaseSimpleRecyclerViewAdapter
import com.ams.currencyconverter.common.data.models.CountryInfo

class CurrenciesListingAdapter(private val clickAction: (item: CountryInfo) -> Unit = {})
    : BaseSimpleRecyclerViewAdapter<CountryInfo, CurrenciesListingViewHolder>(listOf()) {
    override fun getNewViewHolder(parent: ViewGroup, viewType: Int) = CurrenciesListingViewHolder(parent, clickAction)
}