package com.ams.currencyconverter.common.data.database.daos

import androidx.room.*
import com.ams.currencyconverter.common.data.models.CountryInfo
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe

@Dao
interface CountriesInfoDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addAll(countriesInfo: List<CountryInfo>): Completable

    @Query("Select * from CountriesInfoTable")
    fun getAll(): Maybe<List<CountryInfo>>
}