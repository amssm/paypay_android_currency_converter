package com.ams.currencyconverter.common.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ams.currencyconverter.common.data.database.AppDatabaseClient

@Entity(tableName = AppDatabaseClient.DB_TABLES.CountriesInfoTable)
data class CountryInfo(@PrimaryKey
                       val currencyName: String,
                       var currencyRate: Float,
                       var flag: String? = null) {

    fun with(inputAmount: Float, targetConversionRate: Float): CountryInfo {
        this.currencyRate = (inputAmount/targetConversionRate) * currencyRate
        return this
    }
}

