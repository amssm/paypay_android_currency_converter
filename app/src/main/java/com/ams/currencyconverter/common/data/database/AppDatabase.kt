package com.ams.currencyconverter.common.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ams.currencyconverter.common.data.database.daos.CountriesInfoDAO
import com.ams.currencyconverter.common.data.models.CountryInfo

@Database(entities = [CountryInfo::class], version = AppDatabaseClient.version, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun getCountriesInfoDao(): CountriesInfoDAO
}
