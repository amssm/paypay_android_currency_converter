package com.ams.currencyconverter.common.data.rest.apis

import com.ams.currencyconverter.common.data.rest.Endpoints
import com.ams.currencyconverter.common.data.models.CountryCurrenciesAndFlag
import com.ams.currencyconverter.common.data.models.CurrenciesResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

interface APIs {

    @GET(Endpoints.FLAGS)
    fun getCountriesFlags(): Observable<List<CountryCurrenciesAndFlag>>

    @GET(Endpoints.CURRENCIES)
    fun getCountriesCurrencies(): Observable<CurrenciesResponse>
}