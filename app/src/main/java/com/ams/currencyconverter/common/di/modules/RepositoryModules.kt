package com.ams.currencyconverter.common.di.modules

import com.ams.currencyconverter.ui.views.common.CurrenciesListingRepo
import org.koin.core.module.Module
import org.koin.dsl.module

val repositoryModules: Module = module {
    factory { CurrenciesListingRepo(apIs = get(), countriesInfoDAO = get()) }
}