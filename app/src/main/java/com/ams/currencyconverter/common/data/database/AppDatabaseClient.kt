package com.ams.currencyconverter.common.data.database

import android.content.Context
import com.ams.androiddevkit.baseClasses.roomDB.BaseDatabaseClient
import com.ams.currencyconverter.BuildConfig

class AppDatabaseClient(context: Context): BaseDatabaseClient<AppDatabase>(context, AppDatabase::class.java) {

    override fun getDatabaseName() = BuildConfig.DATABASE_NAME

    companion object DatabaseConfigs {
        const val version = 1
    }

    @Suppress("ClassName")
    annotation class DB_TABLES {
        companion object {
            const val CountriesInfoTable = "CountriesInfoTable"
        }
    }
}


