package com.ams.currencyconverter.common.di.modules

import com.ams.currencyconverter.common.data.database.AppDatabase
import com.ams.currencyconverter.common.data.database.AppDatabaseClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

val databaseClientModule: Module = module {
    single { AppDatabaseClient(androidContext()).getDatabase() }
}

val databaseDAOsModules: Module = module {
    factory { get<AppDatabase>().getCountriesInfoDao() }
}
