package com.ams.currencyconverter.common.application

import com.ams.androiddevkit.baseClasses.application.BaseAppLifecycleObserver
import com.ams.currencyconverter.backgroundFetchers.DataUpdateWorker
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.disposables.Disposable
import java.util.concurrent.TimeUnit

class AppLifecycleObserver: BaseAppLifecycleObserver() {

    private lateinit var timer: Disposable

    override fun onAppEnterForeground() {
        super.onAppEnterForeground()
        if(::timer.isInitialized) {
            this.timer.dispose()
        }
    }

    override fun onAppEnterBackground() {
        super.onAppEnterBackground()
        // When the app goes to background wait for 5 seconds then start the work manager,
        // I'm doing this basically to make sure that it is not called many times
        // if the user is switching rapidly between the foreground and the background.
        this.timer = Completable.timer(5, TimeUnit.SECONDS).subscribe {
            DataUpdateWorker.run()
        }
    }
}