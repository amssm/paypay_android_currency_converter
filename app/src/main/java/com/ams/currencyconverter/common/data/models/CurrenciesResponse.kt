package com.ams.currencyconverter.common.data.models

data class CurrenciesResponse(val success: Boolean,
                              val error: Map<String, Any>?,
                              val quotes: Map<String, Float>?)

