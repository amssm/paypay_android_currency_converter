package com.ams.currencyconverter

import com.ams.androiddevkit.baseClasses.networking.IRetrofitClient
import com.ams.currencyconverter.common.data.database.AppDatabaseClient
import com.ams.currencyconverter.common.data.database.daos.CountriesInfoDAO
import com.ams.currencyconverter.common.data.rest.apis.APIs
import com.ams.currencyconverter.testingUtils.TestData
import com.ams.currencyconverter.stubs.UnitTestingRestClient
import com.ams.currencyconverter.ui.views.common.CurrenciesListingRepo
import org.junit.Rule
import org.junit.Test
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.get
import org.koin.test.mock.MockProviderRule
import org.koin.test.mock.declareMock
import org.mockito.BDDMockito.given
import org.mockito.Mockito.mock

class RepoUnitTests: KoinTest {

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(
            module {
                single { UnitTestingRestClient() }.bind(IRetrofitClient::class)
                factory { get<IRetrofitClient>().getRetrofitClient(APIs::class.java) }
                single { mock(AppDatabaseClient::class.java) }
                factory { mock(CountriesInfoDAO::class.java) }
                single { CurrenciesListingRepo(apIs = get(), countriesInfoDAO = get()) }
            })
    }

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz -> mock(clazz.java) }

    @Test
    fun mergingDataIsCorrect() {
        declareMock<APIs> { given(this.getCountriesCurrencies()).willReturn(TestData.getCountriesCurrenciesSuccessResponse()) }
        declareMock<CountriesInfoDAO> { given(this.getAll()).willReturn(TestData.getCountriesListSuccessResponse()) }
        get<CurrenciesListingRepo>().getCurrenciesRatesWithFlags()
            .test()
            .assertValue {
                it.size == 3 && it.find { country -> country.currencyName == "EGP" && country.currencyRate == 15.842604F } != null
            }
            .dispose()
    }
}