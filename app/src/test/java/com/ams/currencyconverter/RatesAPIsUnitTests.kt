package com.ams.currencyconverter

import com.ams.androiddevkit.utils.services.gsonService.GsonService
import com.ams.androiddevkit.utils.services.gsonService.GsonServiceImpl
import com.ams.androiddevkit.utils.services.serialization.GsonSerializationService
import com.ams.androiddevkit.utils.services.serialization.SerializationService
import com.ams.currencyconverter.common.data.rest.apis.APIs
import com.ams.currencyconverter.common.exceptionsAndThrowables.APIError
import com.ams.currencyconverter.mocks.ApiServiceMock
import org.junit.Rule
import org.junit.Test
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.get
import org.koin.test.mock.MockProviderRule
import org.mockito.Mockito

class RatesAPIsUnitTests: KoinTest {

    private var shouldMockErrors = false

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(
                module {
                    factory { GsonServiceImpl() }.bind(GsonService::class)
                    single { GsonSerializationService() }.bind(SerializationService::class)
                    factory { ApiServiceMock(shouldMockErrors, get()) }.bind(APIs::class)
                })
    }

    @get:Rule
    val mockProvider = MockProviderRule.create { clazz -> Mockito.mock(clazz.java) }

    @Test
    fun successResponseParsingIsCorrect() {
        this.shouldMockErrors = false
        get<APIs>()
                .getCountriesCurrencies()
                .test()
                .assertValue {
                    it.success && it.error == null && it.quotes != null
                }
                .dispose()
    }

    @Test
    fun errorResponseParsingIsCorrect() {
        this.shouldMockErrors = true
        get<APIs>()
                .getCountriesCurrencies()
                .test()
                .assertError { throwable ->
                    (throwable as APIError).let {
                        !it.success
                    }
                }
            .dispose()
    }
}