package com.ams.currencyconverter.testingUtils

object JSONUtils {

    fun loadJSON(fileName: String): String? {
        return javaClass.classLoader?.getResource(fileName)?.readText()
    }
}