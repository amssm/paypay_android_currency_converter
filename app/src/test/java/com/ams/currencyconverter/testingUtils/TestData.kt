package com.ams.currencyconverter.testingUtils

import com.ams.currencyconverter.common.data.models.CountryInfo
import com.ams.currencyconverter.common.data.models.CurrenciesResponse
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable

object TestData {
    fun getCountriesCurrenciesSuccessResponse(): Observable<CurrenciesResponse> {
        val response = CurrenciesResponse(success = true,
            error = null,
            quotes = mapOf("USDAED" to 3.673204F, "USDEGP" to 15.842604F, "USDUSD" to 1F))
        return Observable.just(response)
    }

    fun getCountriesListSuccessResponse(): Maybe<List<CountryInfo>> {
        val aed = CountryInfo("AED", 3.673204F, "https://restcountries.eu/data/egy.svg")
        val egp = CountryInfo("EGP", 15.842604F, "https://restcountries.eu/data/are.svg")
        val usa = CountryInfo("USD", 1F, "https://restcountries.eu/data/asm.svg")
        val response = listOf(egp, aed, usa)
        return Maybe.just(response)
    }
}