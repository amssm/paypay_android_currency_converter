package com.ams.currencyconverter

import com.ams.currencyconverter.common.data.models.CountryInfo
import org.junit.Test

class CurrencyConverterTests {

    @Test
    fun testLocalCurrencyConversion() {
        // 1 USD to EGP
        val usdToEgpRate = 15.70F
        // 1 USD to AED
        val usdToAedRate = 3.67F
        // Expected Result
        // 1 AED to EGP
        val aedToEgpRate = 4.28F
        //
        val usdCurrencyInfo = CountryInfo(currencyName = "USD", currencyRate = usdToEgpRate, flag = null)
        // AED to EGP
        val convertedCurrencyInfo = usdCurrencyInfo.with(inputAmount = 1F, targetConversionRate = usdToAedRate)
        // Rounded to two decimal places
        val resultRate = this.round(convertedCurrencyInfo.currencyRate, 2)
        //
        assert(aedToEgpRate.toString() == resultRate)
    }

    @Suppress("SameParameterValue")
    private fun round(number: Float, decimalPlaces: Int): String {
        return "%.${decimalPlaces}f".format(number)
    }
}