package com.ams.currencyconverter.mocks

import com.ams.androiddevkit.utils.services.serialization.SerializationService
import com.ams.currencyconverter.common.data.models.CountryCurrenciesAndFlag
import com.ams.currencyconverter.common.data.models.CurrenciesResponse
import com.ams.currencyconverter.common.data.rest.apis.APIs
import com.ams.currencyconverter.common.exceptionsAndThrowables.APIError
import com.ams.currencyconverter.testingUtils.JSONUtils
import io.reactivex.rxjava3.core.Observable

class ApiServiceMock(private val shouldMockErrors: Boolean,
                     private val serializationService: SerializationService): APIs {

    override fun getCountriesFlags(): Observable<List<CountryCurrenciesAndFlag>> {
        // ToDo:- To implement if I finished on time
        return Observable.error(Throwable("To implement if I finished on time"))
    }

    override fun getCountriesCurrencies(): Observable<CurrenciesResponse> {
        return if(shouldMockErrors) {
            val response = JSONUtils.loadJSON("CurrenciesRatesErrorResponse.json")!!
            val obj = serializationService.fromJson(response, APIError::class.java)
            Observable.error(obj)
        }
        else {
            val response = JSONUtils.loadJSON("CurrenciesRatesSuccessResponse.json")!!
            val obj = serializationService.fromJson(response, CurrenciesResponse::class.java)
            Observable.just(obj)
        }
    }
}