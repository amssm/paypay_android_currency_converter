package com.ams.currencyconverter.stubs

import com.ams.androiddevkit.baseClasses.networking.RetrofitClient
import com.ams.currencyconverter.BuildConfig
import okhttp3.HttpUrl.Companion.toHttpUrl
import retrofit2.Converter
import retrofit2.Retrofit

open class UnitTestingRestClient(private val jsonConverter: Converter.Factory? = null): RetrofitClient() {

    override fun getBaseURL() = BuildConfig.BASE_URL

    override fun getRetrofitBuilder(): Retrofit.Builder {
        return super.getRetrofitBuilder()
                .baseUrl("")
                .baseUrl("https://ourapi.com/".toHttpUrl())
    }

    override fun isDebuggable() = true

    public override fun getConverterFactory(): Converter.Factory? = jsonConverter
}