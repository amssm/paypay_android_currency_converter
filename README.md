# PayPayAndroidCurrencyConverter

1. I used MVVM (Model-View-View-Model).

2. I also used [Android dev Kit](https://github.com/ahmadmssm/AndroidBase), which is my open-source Android library that facilitates development, it is also a wrapper around Retrofit, OkHTTP, and RxJava/Android plus some useful utilities.

3. [Koin](https://github.com/InsertKoinIO/koin) is used as a dependency injection framework.

4. Along with the provided [Currencies rates API](https://currencylayer.com), I used this [API](https://restcountries.eu/) to fetch the world countries basically to get the countries flags, I then combined the results from the two APIs to get a new list that has both the flag and currency info.

5. To update the data every 30 minutes, I used WorkManager which should run every 30 minutes to keep the rates updated, the data is cached in **Room database** to limit the bandwidth usage.

6. Used [Mockito](https://github.com/mockito/mockito) with unit testing.

7. All the icons in the app are from [FlatIcon](https://www.flaticon.com/), they are all free without copyright.

8. I wanted to include more unit tests but the time is very tight for me as I'm very busy with shipping a feature to about 4.7 Million users, that will help them with the COVID-19 situation.


### Screenshots :

![](https://gitlab.com/amssm/paypay_android_currency_converter/-/raw/master/Screenshots/Listing.png "Currencies rates list").
![](https://gitlab.com/amssm/paypay_android_currency_converter/-/raw/master/Screenshots/Search.png "Currencies rates list with search for currency(ies)").
![](https://gitlab.com/amssm/paypay_android_currency_converter/-/raw/master/Screenshots/Convert.png "Convert a currency to other currencies").
![](https://gitlab.com/amssm/paypay_android_currency_converter/-/raw/master/Screenshots/Reset.png "Reset convert a currency to other currencies").

Thank you very much...
