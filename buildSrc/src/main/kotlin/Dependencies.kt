object Dependencies {
    const val testRunner = "androidx.test:runner:${Versions.testRunner}"
    const val mockito = "org.mockito:mockito-core:${Versions.mockito}"
    const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinVersion}"
    const val ktxCore = "androidx.core:core-ktx:${Versions.androidXCore}"
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val material = "com.google.android.material:material:${Versions.material}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val androidDevKit = "com.github.ahmadmssm:AndroidBase:${Versions.androidDevKit}"
    const val retrofitGsonAdapter = "com.squareup.retrofit2:converter-gson:${Versions.retrofitGsonAdapter}"
    const val rxJava = "io.reactivex.rxjava3:rxandroid:${Versions.rxJava}"
    const val rxAndroid = "io.reactivex.rxjava3:rxjava:${Versions.rxAndroid}"
    const val rxBinding = "com.jakewharton.rxbinding4:rxbinding-appcompat:${Versions.rxBinding}"
    const val koin = "org.koin:koin-core:${Versions.koin}"
    const val room = "androidx.room:room-runtime:${Versions.room}"
    const val roomKAPT = "androidx.room:room-compiler:${Versions.room}"
    const val roomRx = "androidx.room:room-rxjava3:${Versions.room}"
    const val rxWorkManager = "androidx.work:work-rxjava3:${Versions.workManager}"
    const val koinViewModel = "org.koin:koin-androidx-viewmodel:${Versions.koin}"
    const val koinFragment = "org.koin:koin-androidx-fragment:${Versions.koin}"
    const val koinUnitTest = "org.koin:koin-test:${Versions.koin}"
    const val coil = "io.coil-kt:coil:${Versions.coilImageLoader}"
    const val coilSVG = "io.coil-kt:coil-svg:${Versions.coilImageLoader}"

}

