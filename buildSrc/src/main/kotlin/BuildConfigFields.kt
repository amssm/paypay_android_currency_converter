object BuildConfigFields {
    const val stringField = "String"
    const val baseUrlField = "BASE_URL"
    const val baseCountriesFlagsUrl = "COUNTRIES_FLAGS_URL"
    const val databaseName = "DATABASE_NAME"
    const val apiKey = "API_KEY"
    const val serverMode = "serverMode"
}